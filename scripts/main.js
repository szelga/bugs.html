var states = [
    {"code": "A", "name": "Ready"},
    {"code": "B", "name": "Design"},
    {"code": "C", "name": "Development"},
    {"code": "D", "name": "Test"},
    {"code": "E", "name": "Release"}
];

var getTaskById = function(id){
  var length = tasks.length,
              i;
  for (i = 0; i < length; i++) {
      if (tasks[i].id == id)
        return tasks[i];
  }
}

var renameMilestone = function(oldName, newName){
  var length = tasks.length,
              i,
              res = false;
  for (i = 0; i < length; i++) {
      if (tasks[i].milestone === oldName){
        tasks[i].milestone = newName;
        res = true;
      }
  }
  return res;
}

var changeTaskId = function(oldId,newId){
  var length = tasks.length,
              i,
              res = false;
  for (i = 0; i < length; i++) {
      if (tasks[i].id === oldId){
        tasks[i].id = newId;
        res = true;
      }
  }
  return res;
}

var getStateName = function(code){
 var length = states.length,
              i;
  for (i = 0; i < length; i++) {
      if (states[i].code === code)
        return states[i].name;
  }
}

var sortedTasks = function (){
return tasks.sort(function(a,b) {
      var v = a.project.localeCompare(b.project);
      if(v==0){
        var m = a.milestone.localeCompare(b.milestone);
        if(m==0){
          return a.id - b.id;
        }else{
          return m;
        }
      }else{
        return v; 
      }
  });
}

var listTasks = function(){
  $('#open-task-list').find("tr:gt(0)").remove();//clear rows
  $('#closed-task-list').find("tr:gt(0)").remove();//clear rows
  
  var ts = sortedTasks();
  
  var length = ts.length,
               task = null,
               i;
  for (i = 0; i < length; i++) {
      task = ts[i];
      var taskElement = 
          $('<tr id="' + task.id + '" class="taskrow" data-desc="'+task.description+'"><td class="readonly"><a href="'+window.location.pathname+'?id='+ task.id +'">' + task.id + "</a></td><td class='readonly'>"+ task.title + "</td><td class='readonly'>" + task.project + "</td><td class='readonly'>" + task.milestone + "</td><td class='readonly'>" +  getStateName(task.state) + "</td><td class='readonly'>" +  task.type + '</td></tr>');
      if(task.state === 'E'){
        $('#closed-task-list').append(taskElement);
      }else{
        $('#open-task-list').append(taskElement);
      }
  }
}

var updateProgress = function(currentProject, currentMilestone){
   var length = tasks.length,
               barWidth,
               task = null,
               i, 
               milestoneTotal = 0, 
               closedTotal = 0;
               startedTotal = 0;
   for (i = 0; i < length; i++) {
      task = tasks[i];
      if(currentProject === task.project && currentMilestone === task.milestone){
        milestoneTotal++;
        if($.inArray(task.state, (["D","E"])) != -1){
          startedTotal++;
          if(task.state === "E"){
            closedTotal++;
          }
        }
      }
  }            
  barWidth = $('#progress').width();          
  $('#progress-fill').css('width',closedTotal/milestoneTotal * barWidth);
  $('#progress-started-fill').css('width',startedTotal/milestoneTotal * barWidth);           
}

var drawTasks = function(currentProject, currentMilestone){
  
  var length = tasks.length,
               task = null,
               i, classes, taskElement;
  for (i = 0; i < length; i++) {
      task = tasks[i];
      if(currentProject === task.project && currentMilestone === task.milestone){
        classes = 'box box_' + task.state; 
        taskElement = 
        $('<li id="'+task.id+'"><div class="'+classes+'"  data-id="'+task.id+'" data-desc="'+task.description+'"><div class="'+task.type+'" title="'+task.type+'"></div><p class="title">' + task.id + "</p>" + task.title + '</div></li>');
        $('#'+task.state).append(taskElement);
      }
  }
  makeBoardDraggable();
  updateProgress(currentProject, currentMilestone);
}

var initMilestones = function(currentProject){
  
  $("#milestones").empty();
  $("#milestone-options").empty();
  var revs = [],
             length = tasks.length,
             i
             , milestone;
  for (i=0, len=length; i<len; i++ ) {
      milestone = tasks[i].milestone;
      if(currentProject === tasks[i].project && $.inArray(milestone,revs) == -1){
        $("#milestones").append($("<option value='"+milestone+"'>"+milestone+"</option>"));
        $("#milestone-options").append($("<option value='"+milestone+"'>"+milestone+"</option>"));
        revs.push(milestone);
      }
  }
  
  $("#milestones option").sort(function(a, b){return ($(b).text()) < ($(a).text());}).appendTo("#milestones");
      
  return revs;
}

var initProjects = function(){
  
  $("#projects").empty();
  $("#project-options").empty();
  var projects = [],
             length = tasks.length,
             i
             , project;
  for (i=0, len=length; i<len; i++ ) {
      project = tasks[i].project;
      if($.inArray(project,projects) == -1){
        $("#projects").append($("<option value='"+project+"'>"+project+"</option>"));
        $("#project-options").append($("<option value='"+project+"'>"+project+"</option>"));
        projects.push(project);
      }
  }
  
  $("#projects option").sort(function(a, b){return ($(b).text()) < ($(a).text());}).appendTo("#projects");
 
 
  if($.inArray(readValue('bugs-project'),projects) == -1){
    setValue("bugs-project",projects[0]);
  }
  
  return projects;
}
  
var createBoard = function() {
  $('#kanban').html('');
  var table = $("<div id=\"board\"></div>");
  
  var length = states.length,
  state = null;
  var i;
  for (i = 0; i < length; i++) {
    state = states[i];
    
    var column = $("<div class=\"dp14\"></div>");
    column.append($("<div class=\"headline\">" + state.name + "</div>"));
    column.append($("<ul class=\"state\" id=\"" + state.code + "\"></ul>"));
    
    table.append(column);
  }
  
  $("#kanban").append(table);
}

var removeTask = function(id){
var t = getTaskById(id);
tasks.splice(tasks.indexOf(t),1);
listTasks();
}

var makeBoardDraggable = function(){
   $('.state', $('#board')).dragsort({ dragBetween: true, dragEnd: function(){
      var data = $(".state").has($(this));
      getTaskById($(this).attr("id")).state = data.attr('id');
      saveChanges();
      updateProgress($("#projects").find(":selected").val(), $("#milestones").find(":selected").val());
   }});
}

var drawKanban = function(){
  createBoard();
  drawTasks($("#projects").find(":selected").val(), $("#milestones").find(":selected").val());
}

var setCookie = function(n,v){document.cookie = n + '=' + v + '; path=/;';}
var readCookie = function(n){return(n=new RegExp('(?:^|;\\s*)'+(''+n).replace(/[-[\]{}()*+?.,\\^$|#\s]/g,'\\$&')+'=([^;]*)').exec(document.cookie))&&n[1];}
var setValue = function(key, value){
  if('localStorage' in window && window['localStorage'] !== null && localStorage){
    localStorage.removeItem(key);
    localStorage.setItem(key,value);
    return true;
  }else{
    setCookie(key,value);
    return false;
  }
}
var readValue = function(key){
  if('localStorage' in window && window['localStorage'] !== null && localStorage){
    return localStorage.getItem(key);
  }else{
    return readCookie(key);
  }
}

var getUrlParamByName = function(name) {
var match = RegExp("[?&]" + name + "=([^&]*)").exec(window.location.search);
return match && decodeURIComponent(match[1].replace(/\+/g, " "));
}

var checkUrlParams = function(){
  var param = getUrlParamByName('id');
  if (param){
    var t = getTaskById(param);
    highlightTask(t);
  }
  
  param = getUrlParamByName('new');
  if (param){
    $("#data_link").click();
    $("#new_task_btn").click();
    $("#new_item_title").focus();
  }
}

var highlightTask = function(t){
$("#data_link").click();
if(t){
  viewTask(t);
  $(".input").addClass("highlight").delay(1000).queue(function(){ $(this).removeClass('highlight'); });
}else{
  alert("Task not found.");
}
}

var viewTask = function(t){     
  $("#new_item_id").val(t.id);
  $("#new_item_title").val(t.title);
  $("#new_item_desc").val(t.description);
  $("#new_item_type").val(t.type);
  $("#new_item_project").val(t.project);
  $("#new_item_mile").val(t.milestone);
}

var nextId = function(){
var max = '0'
    , length = tasks.length
    , i;
for(i=0;i<length;i++){
  if(Number(max) < Number(tasks[i].id)){
    max = tasks[i].id;
  }
}
if(parseInt(max,10)){
  return parseInt(max) + 1;
}else{
  return max.substring(0, len - 1) + String.fromCharCode(max.charCodeAt(max.length-1) + 1);
}
}

var saveChanges = function(){
if(useAutomaticStorage){
  if(! setValue("bugs-tasks", JSON.stringify(tasks))){
    $("#storage-notice").text("Using cookies for storage");
  }
}
}

$(function(){
  if(title){
    $('#title').html(title);
    document.title = title + " - bugs";
  }
  if(logo){
      $("#logo").attr('src',logo);
  }
  
  $("#board_link").hide();
  $("#data_wrapper").hide();
        
  $("#board_link, #logo").click(function() {
    $("#board_wrapper").show();
    $("#data_link").show();
    $("#data_wrapper").hide();
    $("#board_link").hide();
    projects = initProjects();
    milestones = initMilestones(readValue('bugs-project'));
    $("#projects").val(readValue('bugs-project'));
    $("#milestones").val(readValue('bugs-milestone'));
    drawKanban(); 
  })

  $("#data_link").click(function() {
    $("#board_wrapper").hide();
    $("#data_link").hide();
    $("#data_wrapper").show();
    $("#board_link").show();
    listTasks();
  });
  
  $("#show-closed-tasks-link").click(function(){
    $("#closed-task-list-wrapper").toggle();
  });
  
  $("#milestones").change(function(){
    setValue("bugs-milestone", $("#milestones").find(":selected").val());
    $(".box").remove();
    drawKanban();
  });
  
  $("#projects").change(function(){
    setValue("bugs-project", $("#projects").find(":selected").val());
    milestones = initMilestones(readValue('bugs-project'));
    $(".box").remove();
    drawKanban();
  });
  
  $("#rename_milestone_btn").click(function(){
      var res = prompt("Enter milestone's current name and new name separated by a comma.\nEx. milestone-1,First-Version");
      if(res!==null){
        res = res.split(',');
        var oldName = $.trim(res[0]);
        var newName = $.trim(res[1]);
        if(renameMilestone(oldName, newName)){
          saveChanges();
          listTasks();
          alert('Milestone "'+ oldName +'" renamed to "'+ newName+'".');
        }else{
          alert('No milestone with the name "'+ oldName +'" was found.');
        }
      }
  });
  
  $("#change_task_id_btn").click(function(){
      var res = prompt("Enter task's current ID and new ID separated by a comma.\nEx. 1,One");
      if(res!==null){
        res = res.split(',');
        var oldId = $.trim(res[0]);
        var newId = $.trim(res[1]);
        if(changeTaskId(oldId, newId)){
          saveChanges();
          listTasks();
          alert('Task ID "'+ oldId +'" changed to "'+ newId+'".');
        }else{
          alert('No task with the ID "'+ oldId +'" was found.');
        }
      }
  });
  
  $("#new_task_btn").click(function(){
      $(".input").val('');
      $("#new_item_id").val(nextId());
  });
  
  $("#save_task_btn").click(function(){
    if( $.trim($("#new_item_id").val()) != "" &&$.trim($("#new_item_title").val()) != ""){
      if( typeof getTaskById($("#new_item_id").val()) == 'undefined' ){
        var task = {
          id: $.trim($("#new_item_id").val()),
          title: $.trim($("#new_item_title").val()),
          description: $.trim($("#new_item_desc").val()),
          type: $.trim($("#new_item_type").val()),
          project: $.trim($("#new_item_project").val()),
          milestone: $.trim($("#new_item_mile").val()),
          state: 'A'
        };
        tasks.push(task);
      }else{
        if(confirm('A task with the ID "'+$("#new_item_id").val()+'" already exists.\n\nClick OK to modify it, or click Cancel and use another ID.')){
          var t = getTaskById($("#new_item_id").val());
          t.title =  $.trim($("#new_item_title").val());
          t.description = $.trim($("#new_item_desc").val());
          t.type = $.trim($("#new_item_type").val());
          t.project = $.trim($("#new_item_project").val());
          t.milestone = $.trim($("#new_item_mile").val());
        }
      }
      initMilestones(readValue('bugs-project'));
      saveChanges();
      listTasks();
    }else{
      alert('The ID and a the Title are required.');
    }
  });
  
  $("#get_data_btn").click(function(){
      alert(JSON.stringify(tasks));
  });
  
  $(window).on('beforeunload',function() {
    if(!useAutomaticStorage){
      return "You're not using automatic storage!\nDon't forget to save your changes manually.";
    }
  });
  
  $(document).on('mouseover','.box',function(e){
      var title = $(this).data('desc');
      $('<p class="tooltip"></p>').text(title).appendTo('body').delay(800).fadeIn('slow');
      var mousex = e.pageX + 20; //Get X coordinates
      var mousey = e.pageY + 10; //Get Y coordinates
      $('.tooltip').css({ top: mousey, left: mousex })
  }).on('mouseleave','.box',function(){
      $('.tooltip').remove();
  }).on('mousedown','.box',function(){
      $('.tooltip').remove();
  }).on('dblclick','.box',function(){
      var t = getTaskById($(this).data('id'));
      highlightTask(t);
  }).on('dblclick','.taskrow',function(){
      if(confirm('Confirm to delete task with ID ' + $(this).attr('id')+'?')){
        removeTask($(this).attr('id'));
        saveChanges();
      }
  }).on('mouseover','.taskrow',function(){
      $(this).addClass('border');
  }).on('mouseleave','.taskrow',function(){
      $(this).removeClass('border');
  }).on('click','.taskrow',function(){
      var t = getTaskById($(this).attr('id'));
      viewTask(t)
  });
        
  if(!tasks || tasks.length < 1){
    useAutomaticStorage = true;
    tasks  = JSON.parse(readValue("bugs-tasks")) || [];
    //$("#storage-notice").text("Using automatic storage");
  }else{
    $("#storage-notice").text("Using manual storage");
  }      
  
  projects = initProjects();                        
  milestones = initMilestones(readValue('bugs-project'));
  $("#projects").val(readValue('bugs-project'));
  $("#milestones").val(readValue('bugs-milestone'));
  drawKanban();
  checkUrlParams();
});
